<?php


use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Models\Shift;
use App\Models\User;
use Faker\Factory;


class AddSingleShiftTest extends TestCase
{
    use DatabaseTransactions;

    public function test_that_a_shift_can_be_added()
    {

        $user = User::factory()->create();
        $faker = Factory::create();
        $startTimes = ["00:00","08:00","16:00"];


        //testing creation of a single shift
        $createShift = $this->actingAs($user)
            ->post('/shifts/add', [
            'date' => $faker->date("d-m-Y"),
            'shift_start_time' => $startTimes[array_rand($startTimes)]
        ]);
        $createShift->assertEquals(201, $createShift->response->status());
        $createShift->seeJson([
            'success' => true,
        ]);


        //testing that the shift was created by getting the shift list
        $getShifts = $this->actingAs($user)->get('/shifts');
        $getShifts->assertEquals(200, $getShifts->response->status());
        $this->assertArrayHasKey('data', $getShifts->response);
        $this->assertIsArray( $getShifts->response->json("data"));


    }

}
