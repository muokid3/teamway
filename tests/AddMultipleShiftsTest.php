<?php


use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Models\Shift;
use App\Models\User;
use Faker\Factory;


class AddMultipleShiftsTest extends TestCase
{
    use DatabaseTransactions;

    public function test_that_multiple_shifts_per_day_cant_be_added()
    {

        $user = User::factory()->create();
        $startTimes = ["00:00","08:00","16:00"];


        //testing creation of the first shift for today
        $createFirstShift = $this->actingAs($user)
            ->post('/shifts/add', [
            'date' => Carbon::today(),
            'shift_start_time' => $startTimes[array_rand($startTimes)]
        ]);
        $createFirstShift->assertEquals(201, $createFirstShift->response->status());
        $createFirstShift->seeJson([
            'success' => true,
        ]);


        //testing creation of the second shift for today
        $createSecondShift = $this->actingAs($user)
            ->post('/shifts/add', [
            'date' => Carbon::today(),
            'shift_start_time' => $startTimes[array_rand($startTimes)]
        ]);
        $createSecondShift->assertEquals(422, $createSecondShift->response->status());
        $createSecondShift->seeJsonEquals([
            'success' => false,
            'message' => 'You already have a shift for the specified date. Please update or delete it if necessary',
        ]);

    }

}
