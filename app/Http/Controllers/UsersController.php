<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersController extends Controller
{



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);


        if($validator->fails()){
            return response([
                'success' => false,
                'message' => 'Validation errors',
                'errors' =>  implode(",",$validator->messages()->all())
            ], 422);
        }


        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $plainPassword = $request->input('password');
        $user->password = app('hash')->make($plainPassword);
        $user->save();


        $data['token_type'] =  'Bearer';
        $data['token'] =  $user->createToken('Teamway Personal Access Client')->accessToken;
        $data['user'] =  $user;


        return response([
            'success' => true,
            'message' => 'Account created successfully!',
            'data' => $data
        ]);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'password' => 'required|string',
            'email' => 'required',
        ]);

        if($validator->fails()){
            return response([
                'success' => false,
                'message' => 'Validation errors',
                'errors' =>  implode(",",$validator->messages()->all())
            ], 422);
        }


        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Teamway Password Grant Client');


                $response = [
                    'success' => true,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
                    'token' => $token->accessToken,
                    'user' => $user
                ];
                return response($response, 200);
            } else {
                $response = [
                    "success"=>false,
                    "message" => "Login failed. Incorrect email or password."
                ];
                return response($response, 401);
            }
        } else {
            $response = [
                "success"=>false,
                "message" =>'Login failed. Incorrect email or password.'
            ];

            return response($response, 401);
        }
    }

    public function user(Request $request)
    {
        $resp = $request->user();

        $response = [
            "success"=>true,
            "user" =>$resp
        ];

        return response($response, 200);
    }

}
