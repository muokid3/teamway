<?php

namespace App\Http\Controllers;


use App\Models\Shift;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class ShiftController extends Controller
{



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function shifts()
    {
        $user = auth()->user();

//        $shifts = ServiceRequest::where('user_id',$user->id)
//            ->orderBy('id','desc')
//            ->paginate(20);

        $response = [
            "success"=>true,
            "data" =>$user->shifts
        ];

        return response($response, 200);

    }

    public function add_shift(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'date' => 'required|date',
            'shift_start_time' => [
                'required',
                Rule::in(['00:00','08:00','16:00']),
            ],
        ],[
            'shift_start_time.in' => 'The :attribute must be 00:00, 08:00 or 16:00.',
        ]);


        if($validator->fails()){
            return response([
                'success' => false,
                'message' => 'Validation errors',
                'errors' => implode(",",$validator->messages()->all())
            ], 422);
        }

        //get logged-in user
        $user = $request->user();

        $shiftDate = Carbon::parse( $request->date);

        //check if shift exists for the user for this day
        $exists = Shift::where('date', $shiftDate)->where('user_id', $user->id)->first();

        if (!is_null($exists))
            return response([
                'success' => false,
                'message' => 'You already have a shift for the specified date. Please update or delete it if necessary',
            ], 422);


        //create a new shift for the give user and date
        $shiftStartTime = Carbon::createFromFormat('H:i', $request->shift_start_time);
        $shiftEndTime = Carbon::createFromFormat('H:i', $request->shift_start_time)->addHours(8);

        $shift = new Shift();
        $shift->user_id = $user->id;
        $shift->date = $shiftDate;
        $shift->shift_start_time = $shiftStartTime;
        $shift->shift_end_time = $shiftEndTime;
        $shift->save();

        return response()->json([
            'success' => true,
            'message' => 'Your shift has been created successfully',
        ], 201);

    }

    public function update_shift(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'id' => 'required|exists:shifts,id',
            'date' => 'required|date',
            'shift_start_time' => [
                'required',
                Rule::in(['00:00','08:00','16:00']),
            ],
        ],[
            'shift_start_time.in' => 'The :attribute must be 00:00, 08:00 or 16:00.',
            'id.exists' => 'The specified shift does not exist',
        ]);


        if($validator->fails()){
            return response([
                'success' => false,
                'message' => 'Validation errors',
                'errors' => implode(",",$validator->messages()->all())
            ], 422);
        }

        //get logged-in user
        $user = $request->user();

        //get shift
        $shift = Shift::find($request->id);

        //check if authenticated user is the owner
        if ($shift->user_id != $user->id)
            return response()->json([
                'success' => false,
                'message' => 'You are not authorised to access this resource',
            ], 401);

        $shiftDate = Carbon::parse( $request->date);

        //check if shift exists for the user for this day
        $exists = Shift::where('date', $shiftDate)
            ->where('id','<>', $request->id)
            ->where('user_id', $user->id)
            ->first();

        if (!is_null($exists))
            return response([
                'success' => false,
                'message' => 'You already have a shift for the specified date. Please update or delete it if necessary',
            ], 200);


        //create a new shift for the give user and date
        $shiftStartTime = Carbon::createFromFormat('H:i', $request->shift_start_time);
        $shiftEndTime = Carbon::createFromFormat('H:i', $request->shift_start_time)->addHours(8);

        $shift->date = $shiftDate;
        $shift->shift_start_time = $shiftStartTime;
        $shift->shift_end_time = $shiftEndTime;
        $shift->update();

        return response()->json([
            'success' => true,
            'message' => 'Your shift has been updated successfully',
        ], 201);

    }

    public function delete_shift($shift_id)
    {

        //get logged-in user
        $user = auth()->user();

        //check if shift exists
        $shift = Shift::find($shift_id);
        if (is_null($shift))
            return response()->json([
                'success' => false,
                'message' => 'The specified shift was not found',
            ], 404);


        //check if authenticated user is the owner
        if ($shift->user_id != $user->id)
            return response()->json([
                'success' => false,
                'message' => 'You are not authorised to access this resource',
            ], 401);


        //delete the shift
        $shift->delete();

        return response()->json([
            'success' => true,
            'message' => 'Shift has been deleted successfully',
        ], 200);
    }

}
