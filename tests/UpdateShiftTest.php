<?php


use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use App\Models\Shift;
use App\Models\User;
use Faker\Factory;


class UpdateShiftTest extends TestCase
{
    use DatabaseTransactions;

    public function test_that_a_shift_can_be_updated()
    {

        $user = User::factory()->create();
        $faker = Factory::create();
        $startTimes = ["00:00","08:00","16:00"];


        //testing creation of a shift
        $createShift = $this->actingAs($user)
            ->post('/shifts/add', [
            'date' => $faker->date("d-m-Y"),
            'shift_start_time' => $startTimes[array_rand($startTimes)]
        ]);
        $createShift->assertEquals(201, $createShift->response->status());
        $createShift->seeJson([
            'success' => true,
        ]);


        //get the shift and update it
        $shift = Shift::where('user_id', $user->id)->orderBy('id','desc')->first();

        $updateShift = $this->actingAs($user)
            ->put('/shifts/update',[
                'id' => $shift->id,
                'date' => $faker->date("d-m-Y"),
                'shift_start_time' => $startTimes[array_rand($startTimes)]
            ]);


        $updateShift->assertEquals(201, $updateShift->response->status());
        $updateShift->seeJson([
            'success' => true,
        ]);

    }

}
