<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/auth/register','UsersController@register');
$router->post('/auth/login','UsersController@login');

$router->group(['middleware' => 'auth:api'], function () use ($router) {
    $router->get('/auth/user','UsersController@user');

    $router->get('/shifts','ShiftController@shifts');
    $router->post('/shifts/add','ShiftController@add_shift');
    $router->put('/shifts/update','ShiftController@update_shift');
    $router->delete('/shifts/delete/{shift_id}','ShiftController@delete_shift');




});

