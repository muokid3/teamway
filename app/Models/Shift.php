<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{

    public function toArray() {
//        $data = parent::toArray();

        $data['id'] = $this->id;
        $data['date'] = Carbon::parse($this->date)->isoFormat('MMM Do YYYY') ;
        $data['shift_start_time'] = $this->shift_start_time;
        $data['shift_end_time'] = $this->shift_end_time;

        return $data;
    }

}
